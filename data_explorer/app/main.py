"""
    Performs data exploration on provided dataset
"""

import logging
import os
import time

import click
from xpresso.ai.client.controller_client import ControllerClient
from xpresso.ai.client.data_client import config
from xpresso.ai.core.commons.exceptions.xpr_exceptions import *
from xpresso.ai.core.commons.utils.constants import DEFAULT_PROBABILITY_BINS
from xpresso.ai.core.commons.utils.constants import KEY_RUN_NAME, COMPONENT_NAME_KEY
from xpresso.ai.core.commons.utils.generic_utils import move_directory
from xpresso.ai.core.data.automl.abstract_dataset import AbstractDataset
from xpresso.ai.core.data.automl.dataset_type import DatasetType
from xpresso.ai.core.data.automl.structured_dataset import StructuredDataset
from xpresso.ai.core.data.automl.unstructured_dataset import UnstructuredDataset
from xpresso.ai.core.data.exploration.dataset_explorer import Explorer
from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.data.versioning.controller_factory import \
    VersionControllerFactory
from xpresso.ai.core.logging.xpr_log import XprLogger

__all__ = ["DataExploration"]
__author__ = "Ashritha Goramane"

logger = XprLogger("data_explorer", level=logging.INFO)


class DataExploration(AbstractPipelineComponent):
    """
        To push and pull data from pachyderm cluster
    """
    IN_PATH = "in_path"
    OUT_PATH = "out_path"

    # Exploration params
    VALIDITY_THRESHOLD = "validity_threshold"
    BINS = "bins"
    REPO_NAME = "repo_name"
    BRANCH_NAME = "branch_name"
    COMMIT_ID = "commit_id"

    def __init__(self, **kwargs):
        super().__init__(name="DataExplorer")
        self.name = kwargs[COMPONENT_NAME_KEY]
        self.cli_args = {}
        self.arguments = kwargs
        self.fetch_arguments()
        self.dataset = None
        self.explorer = None
        self.repomanager = None
        self.config = config
        self.start_timestamp = None
        self.end_timestamp = None

    def start(self, xpresso_run_name):
        """
        This is the start method, which does the actual data
        preparation.
        As you can see, it does the following:
          - Calls the superclass start method - this notifies the
          Controller that
              the component has started processing (details such as
              the start
              time, etc. are appropriately stored by the Controller)
          - Main data processing or training codebase.
          - It calls the completed method when it is done

        Args:
            xpresso_run_name: xpresso run name which is used by base class to
                identify the current run. It must be passed. While
                running as
                pipeline,
               Xpresso automatically adds it.
        """
        super().start(xpresso_run_name=xpresso_run_name)
        print("Data explorer component starting", flush=True)
        self.explore_dataset()
        print("Data explorer component completed", flush=True)
        self.completed()

    def completed(self, push_exp=False):
        """
        This is the completed method. It stores the output data files on the
        file system, and then calls the superclass completed method, which notes
        the fact that the component has completed processing, along with the
        end time.

        User must need to call super completed method at the end of the method
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned

        """
        super().completed(push_exp=push_exp)

    def send_metrics(self, status):
        """ It is called to report intermediate status. It reports status and
        metrics back to the xpresso.ai controller through the report_status
        method of the superclass. The Controller stores any metrics reported in
        a database, and makes these available for comparison. It needs the
        following format:
        - status:
           - status - <single word description>
        - metric:
           - Key-Value - Of the metrics that needs to be tracked and visualized
                         realtime. This could be data size, accuracy, loss etc.
        """
        report_status = {
            "status": {"status": status},
            "metric": {
                "elapsed_time": self.end_timestamp - self.start_timestamp}
        }
        self.report_status(status=report_status)

    def get_dataset_object(self):
        """Depending upon dataset type get specific dataset object"""
        dataset_type = self.dataset.type
        if dataset_type == DatasetType.STRUCTURED:
            self.dataset = StructuredDataset()
        elif dataset_type == DatasetType.UTEXT:
            self.dataset = UnstructuredDataset()
        else:
            raise InvalidDatatypeException(f"Dataset type {dataset_type} not "
                                           "supported")

    def import_dataset_from_folder(self):
        """Import dataset from in path"""
        self.dataset = AbstractDataset()
        self.dataset.load(self.cli_args[self.IN_PATH])
        # Get dataset object of specific type
        self.get_dataset_object()
        self.dataset.load(self.cli_args[self.IN_PATH])

    def import_dataset_from_version_controller(self):
        """Import dataset from version controller"""
        password = self.config.PASSWORD
        uid = self.config.UID

        self.version_connect(uid, password)
        self.dataset = self.repomanager.pull_dataset(repo_name=self.cli_args[
            self.REPO_NAME], branch_name=self.cli_args[self.BRANCH_NAME],
                                                     commit_id=self.cli_args[
                                                         self.COMMIT_ID])[1]

    def version_connect(self, uid, password):
        """Login to xpresso"""
        try:
            os.system("echo -n {} > ~/.xpr/.workspace".format("default"))
            client = ControllerClient()
            client.login(uid, password)
            controller_factory = VersionControllerFactory()
            self.repomanager = controller_factory.get_version_controller()
        except Exception as exp:
            raise exp

    def initialize_explorer(self):
        """Initialize explorer object"""
        if self.cli_args[self.REPO_NAME]:
            self.import_dataset_from_version_controller()
        elif self.cli_args[self.IN_PATH]:
            self.import_dataset_from_folder()
        try:
            if self.dataset.data.empty:
                print("Unable to import dataset with given config")
        except AttributeError:
            print("Unable to import dataset with given config")
        self.set_explore_params()
        self.explorer = Explorer(self.dataset)

    def explore_dataset(self):
        """Performs exploration on the dataset provided in in path or repo"""
        print("Starting exploration", flush=True)
        self.initialize_explorer()
        logger.info(f"Starting exploration on {self.dataset.name} dataset")
        self.start_timestamp = time.time()
        self.explorer.understand(verbose=False)
        self.end_timestamp = time.time()
        self.send_metrics("understand_data")
        if self.dataset.type == DatasetType.STRUCTURED:
            try:
                self.start_timestamp = time.time()
                self.explorer.explore_univariate(verbose=False, to_excel=True,
                                                 output_path=f"{self.cli_args[self.OUT_PATH]}"
                                                             f"/Exploration/",
                                                 validity_threshold=
                                                 self.cli_args[
                                                     self.VALIDITY_THRESHOLD],
                                                 bins=self.cli_args[self.BINS])
                self.end_timestamp = time.time()
                self.send_metrics("explore_univariate")
            except XprExceptions as e:
                print("Univariate exploration unsuccessful")
            try:
                self.start_timestamp = time.time()
                self.explorer.explore_multivariate(verbose=False, to_excel=True,
                                                   output_path=f"{self.cli_args[self.OUT_PATH]}"
                                                               f"/Exploration/")
                self.end_timestamp = time.time()
                self.send_metrics("explore_multivariate")
            except XprExceptions as e:
                print("Multivariate exploration unsuccessful")
        elif self.dataset.type == DatasetType.UTEXT:
            self.start_timestamp = time.time()
            self.explorer.explore_unstructured(verbose=False)
            self.end_timestamp = time.time()
            self.send_metrics("explore_unstructured")
        self.save_dataset()

    def save_dataset(self):
        """Save dataset into out path"""
        folder_path = self.dataset.save()
        logger.info(
            f"Saving dataset to {self.cli_args[self.OUT_PATH]}")
        move_directory(folder_path, self.cli_args[self.OUT_PATH])
        print(f"Data saved to {self.cli_args[self.OUT_PATH]}")

    def set_explore_params(self):
        """Helper function to set default exploration parameters"""
        if not self.cli_args[self.BINS]:
            self.cli_args[self.BINS] = DEFAULT_PROBABILITY_BINS
        if not self.cli_args[self.BINS]:
            self.cli_args[self.VALIDITY_THRESHOLD] = None

    def extract_argument(self, argument):
        """
        Args:
        argument(str): Name of argument to extract
        Returns:
            argument value or None
        """
        if argument in self.arguments:
            return self.arguments[argument]
        return None

    def fetch_arguments(self):
        """
        Fetch arguments form CLI
        Returns:
            Returns arguments
        """

        arguments_key = [self.BINS, self.REPO_NAME, self.BRANCH_NAME,
                         self.COMMIT_ID, self.VALIDITY_THRESHOLD, self.OUT_PATH,
                         self.IN_PATH]
        for arg in arguments_key:
            self.cli_args[arg] = self.extract_argument(arg)
        self.validate_input()

    def validate_input(self):
        """Validate input arguments"""
        if not self.cli_args[self.OUT_PATH]:
            self.cli_args[self.OUT_PATH] = "/data"
        if not (self.cli_args[self.REPO_NAME] or self.cli_args[self.IN_PATH]):
            print(f"{self.IN_PATH} or {self.REPO_NAME} mandatory")
        if self.cli_args[self.REPO_NAME] and not (
                self.cli_args[self.BRANCH_NAME] and self.cli_args[
            self.COMMIT_ID]):
            print(f"{self.BRANCH_NAME} and {self.COMMIT_ID}"
                  f"mandatory wit repo_name.")


@click.command()
@click.argument(KEY_RUN_NAME)
@click.argument(COMPONENT_NAME_KEY)
@click.option('-in-path', type=str, help='Path of the file to load data from')
@click.option('-out-path', type=str, help='Path of the file to save data from')
@click.option('-validity-threshold', type=int,
              help='percent value for garbage threshold')
@click.option('-bins', type=int, help=' No. of buckets for probability '
                                      'distribution function, default '
                                      '20')
@click.option('-repo-name', type=str, help='Name of the repo')
@click.option('-branch-name', type=str, help='Branch name')
@click.option('-commit-id', type=str, help='Commit Id')
@click.option('-env', '--environment', type=str, help="Workspace on xpresso")
def cli_options(**kwargs):
    result = DataExploration(**kwargs)
    try:
        if KEY_RUN_NAME in kwargs:
            result.start(xpresso_run_name=kwargs[KEY_RUN_NAME])
        else:
            result.start(xpresso_run_name="")
    except Exception as exception:
        click.secho(f"Error:{exception}", err=True, fg="red")


if __name__ == "__main__":
    cli_options()
